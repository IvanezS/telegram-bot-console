﻿using System;
using System.Configuration;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace TelegramDrugToRelaxBotConsole
{
    class Program
    {
        private static ITelegramBotClient client;
        static void Main(string[] args)
        {
            string token = ConfigurationManager.AppSettings["TelegramToken"];
            client = new TelegramBotClient(token) { Timeout = TimeSpan.FromSeconds(10)};
            var bot = client.GetMeAsync().Result;
            Console.WriteLine("Bot ID: " + bot.Id + " FirstName " + bot.FirstName + " Username " + bot.Username);

            client.OnMessage += OnMessageEvent;
            client.StartReceiving();

            Console.ReadKey();
        }

        private async static void OnMessageEvent(object sender, MessageEventArgs e)
        {
            
            var text = e?.Message?.Text;
            if (text == null) return;
            Console.WriteLine("Пришло сообщение: " + text + " e?.Message?.MessageId = " + e?.Message?.MessageId);
            string[] answerArray =
            {
                "Может кофейку?",
                "А оно тебе надо, ащемто?",
                "Да забей: донт ворри, би хэппи",
                "Кстати, погодка то - отличнейшая!",
                "Ой, ты опять о старом. Сколько можно?",
                "Кстати, ты сегодня уже поел?",
                "Ляг поспи - пройдёт",
                "Ой, ну началось",
                "Зато ты у мамы молодец!",
                "Посмотри в зеркало - таким кросаффчекам нечего беспокоиться",
                "Навальному бы твои проблемы",
                "Если у тебя нет геммороя, можешь сюда больше не писать",
                "Мда... очередной нытик в чате",
                "Если проблема решается венерологом, значит это не проблема"

            };

            Random rand = new Random();
            int index = rand.Next(answerArray.Length);
            
            var answer = (text == $"/start") ? "Привет. Это бот от Вани и Иры. Накатила грусняшка - опиши свою свою проблему" : answerArray[index];


            await client.SendTextMessageAsync(chatId: e.Message.Chat, text: answer);
            Console.WriteLine("Отвечаем: " + answerArray[index]);
        }
    }
}
